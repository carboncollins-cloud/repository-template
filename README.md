# Repository - Template

[[_TOC_]]

## Description

Repository detailing the common repository structure for repositories found within [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud).
